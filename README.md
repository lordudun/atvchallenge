## Installation

To setup all your environment to execute the schedulre please execute the following command:

make init

## How to run it

To run the service you should execute:

make run

Then the service will be available at http://127.0.0.1:5000/

## Usage

After starting the service you will be able to access in the url http://127.0.0.1:5000/

This is the list of the available endpoints:

- Retrieve the list of tasks: /api/tasks ['GET']
- Create a new task: /api/tasks ['POST']
- Retrieve an especific task by id: /api/tasks/<task_id> ['GET']
- Modify an exisiting task: /api/tasks/<task_id> ['PATCH']
- Delete an existing task: /api/tasks/<task_id> ['DELETE']
- Modify the payload of an existing task: /api/tasks/task_id>/payload ['POST']
- Delete a payload entry of an existing task: /api/tasks/<task_id>/payload/<key> ['DELETE']

For example you can see all the tasks added in the following url:

http://127.0.0.1:5000/api/tasks

## Cloud instance running

You can find a version of this service alreday deploy and ready in the following url:

http://lordudun.pythonanywhere.com/api/tasks