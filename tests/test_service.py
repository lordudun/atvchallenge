#!/usr/bin/env python
from .context import atvscheduler

import unittest
import json

class TestSchedulerOperations(unittest.TestCase):

    def setUp(self):
        atvscheduler.app.testing = True
        self.app = atvscheduler.app.test_client()
        atvscheduler.init()

    def test_retrieve_tasks(self):
        
        expected_output = {'tasks': [{'status': 'SCHEDULED', 'title': 'Task 1',
                            'worker': 'w1', 'payload': {'key2': [1, 2, 3],
                             'key1': 'value1'}, 'start': '20160718T01', 'id':
                              1}, {'status': 'SCHEDULED', 'title': 'Task 2',
                               'worker': 'w1', 'payload': {'key2': [1, 2, 3],
                                'key1': 'value1'}, 'start': '20160718T01',
                                 'id': 2}]}

        response = self.app.get('/api/tasks')
        print(json.loads(response.get_data()))

        self.assertEqual(json.loads(response.get_data()), expected_output)

    def test_when_add_a_new_task(self):

        expected_output = {u'status': u'SCHEDULED', u'title': u'New Task', u'worker':
                             u'W3', u'payload': {u'key2': [1, 2, 3], u'key1':
                              u'value1'}, u'start': u'20160718T01', u'id': 3}
        
        new_task_json = {
            'start': '20160718T01',
            'title': 'New Task',
            'worker': 'W3',
            'status': 'SCHEDULED',
            'payload': {"key1": "value1", "key2":[1,2,3]}
        }

        response = self.app.post('/api/tasks', data=json.dumps(new_task_json),
                                 content_type='application/json')

        print(json.loads(response.get_data()))
        self.assertEqual(json.loads(response.get_data()), expected_output)

    def test_retrieve_task_by_id(self):
        
        expected_output = {u'status': u'SCHEDULED', u'title': u'Task 2',
                             u'worker': u'w1', u'payload': {u'key2':
                              [1, 2, 3], u'key1': u'value1'}, u'start':
                               u'20160718T01', u'id': 2}

        response = self.app.get('/api/tasks/2')
        
        self.assertEqual(json.loads(response.get_data()), expected_output)

    def test_update_existing_task(self):

        expected_output = {u'status': u'RUNNING', u'title': u'Updated Task',
                            u'worker': u'W4', u'payload': {u'key2': [3, 2, 1],
                             u'key1': u'v1'}, u'start': u'20170718T01',
                              u'id': 2}
        
        update_task_json = {
            'start': '20170718T01',
            'title': 'Updated Task',
            'worker': 'W4',
            'status': 'RUNNING',
            'payload': {"key1": "v1", "key2":[3,2,1]}
        }

        response = self.app.patch('/api/tasks/2', data=json.dumps(update_task_json),
                                    content_type='application/json')

        self.assertEqual(json.loads(response.get_data()), expected_output)

    def test_delete_specific_task(self):

        expected_output = {u'response': u'Success'}

        response = self.app.delete('/api/tasks/2')
        self.assertEqual(json.loads(response.get_data()), expected_output)

    def test_update_task_payload_by_id(self):
        
        expected_output = {u'status': u'SCHEDULED', u'title': u'Task 1',
                             u'worker': u'w1', u'payload': {u'key2': [4, 5, 6],
                              u'key1': u'newvalue'}, u'start': u'20160718T01',
                               u'id': 1}

        update_task_payload_json = {'payload': {"key1": "newvalue",
                                     "key2":[4,5,6]}}

        response = self.app.post('/api/tasks/1/payload', 
                                data=json.dumps(update_task_payload_json), 
                                content_type='application/json')

        self.assertEqual(json.loads(response.get_data()), expected_output)

    def test_delete_task_payload_by_id(self):
        
        expected_output = {u'status': u'SCHEDULED', u'title': u'Task 1',
                             u'worker': u'w1', u'payload': {u'key1':
                              u'value1'}, u'start': u'20160718T01', u'id': 1}

        response = self.app.delete('/api/tasks/1/payload/key2')
        self.assertEqual(json.loads(response.get_data()), expected_output)

if __name__ == '__main__':
    unittest.main()