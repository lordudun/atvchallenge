#!/usr/bin/env python
from .context import atvscheduler

import unittest

class TestScheduler(unittest.TestCase):

    def test_task_does_not_exist(self):
        
        scheduler = atvscheduler.Scheduler()
        task = scheduler.get('1')
        self.assertIsNone(task)

    def test_new_task_is_added(self):
        
        scheduler = atvscheduler.Scheduler()
        new_task = atvscheduler.Task('20160718T01', 'W1', 'SCHEDULED')
        scheduler.add(new_task)

        task = scheduler.get(1)
        self.assertEqual(task, new_task)

    def test_add_a_task(self):
        
        scheduler = atvscheduler.Scheduler()
        new_task = atvscheduler.Task('20160718T01', 'W1', 'SCHEDULED')
        task = scheduler.add(new_task)

        self.assertEqual(task, new_task)

    def test_modify_a_task(self):
        
        scheduler = atvscheduler.Scheduler()
        new_task = atvscheduler.Task('20160718T01', 'W1', 'SCHEDULED')
        scheduler.add(new_task)
        new_task.set_worker('W3')

        task = scheduler.modify(new_task, 1)

        self.assertEqual(task, new_task)    

    def test_delete_a_task_result(self):
        
        scheduler = atvscheduler.Scheduler()    
        new_task = atvscheduler.Task('20160718T01', 'W1', 'SCHEDULED')
        scheduler.add(new_task)
        result = scheduler.delete(1)
        
        self.assertTrue(result)

    def test_delete_a_task(self):
        
        scheduler = atvscheduler.Scheduler()
        new_task = atvscheduler.Task('20160718T01', 'W1', 'SCHEDULED')
        scheduler.add(new_task)
        scheduler.delete(1)

        task = scheduler.get(1)
        self.assertIsNone(task)

    def test_not_possible_to_delete_a_task(self):
        
        scheduler = atvscheduler.Scheduler()
        new_task = atvscheduler.Task('20160718T01', 'W1', 'RUNNING')
        scheduler.add(new_task)
        result = scheduler.delete(1)

        self.assertFalse(result)
            
    def test_when_task_is_modified_is_updated(self):
        
        scheduler = atvscheduler.Scheduler()
        new_task = atvscheduler.Task('20160718T01', 'W1', 'SCHEDULED')
        scheduler.add(new_task)
        new_task.set_worker('W6')

        scheduler.modify(new_task, 1)

        task = scheduler.get(1)
        self.assertEqual(task, new_task)

    def test_show_all_tasks_added(self):
        
        expected_output = [{'status': 'SCHEDULED', 'title': None, 'worker':
         'W1', 'id': 1, 'start': '20160718T01', 'payload': {}}, {'status':
          'SCHEDULED', 'title': None, 'worker': 'W2', 'id': 2, 'start': 
          '20160718T01', 'payload': {}}, {'status': 'SCHEDULED', 'title': None, 
          'worker': 'W3', 'id': 3, 'start': '20160718T01', 'payload': {}}]

        scheduler = atvscheduler.Scheduler()
        new_task = atvscheduler.Task('20160718T01', 'W1', 'SCHEDULED')
        scheduler.add(new_task)
        new_task = atvscheduler.Task('20160718T01', 'W2', 'SCHEDULED')
        scheduler.add(new_task)
        new_task = atvscheduler.Task('20160718T01', 'W3', 'SCHEDULED')
        scheduler.add(new_task)

        output = scheduler.get_all_tasks_data()
        
        self.assertEqual(output, expected_output)

if __name__ == '__main__':
    unittest.main()