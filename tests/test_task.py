#!/usr/bin/env python
from .context import atvscheduler

import unittest

class TestTask(unittest.TestCase):

    def test_create_valid_task(self):
        
        task = atvscheduler.Task('20160718T01','W1', 'SCHEDULED')
        
        self.assertIsNotNone(task)

    def test_get_data(self):

        expected_output = {'status': 'SCHEDULED', 'title': 'Task 1', 'worker':
                             'w1', 'id': 0, 'start': '20160718T01', 'payload':
                              {'key2': [1, 2, 3], 'key1': 'value1'}}

        task = atvscheduler.Task('20160718T01','w1','SCHEDULED')
        task.set_title('Task 1')
        task.set_payload({"key1": "value1", "key2":[1,2,3]})
        
        output = task.get_data()
        self.assertEquals(output, expected_output)

    def test_update_from_other_task(self):

        task = atvscheduler.Task('20160718T01','w1','SCHEDULED')
        task.set_title('Task 1')
        task.set_payload({"key1": "value1", "key2":[1,2,3]})

        modified_task = atvscheduler.Task('20160718T01','w1','SCHEDULED')
        modified_task.set_title('New Title')
        modified_task.set_payload({"key1": "value1", "key2":[1,2,3]})

        task.update_from(modified_task)

        self.assertEquals(task, modified_task)

    def test_not_valid_status(self):
        
        with self.assertRaises(ValueError):
            task = atvscheduler.Task('20160718T01','w1','NOTVALID')

    def test_not_valid_woker(self):
        
        with self.assertRaises(ValueError):
            task = atvscheduler.Task('20160718T01','', 'SCHEDULED')

    def test_none_start(self):
        
        with self.assertRaises(ValueError):
            task = atvscheduler.Task(None,'WW', 'SCHEDULED')

    def test_no_valid_start_date(self):
        
        with self.assertRaises(ValueError):
            task = atvscheduler.Task('','w1', 'SCHEDULED')

    def test_is_running(self):
        task = atvscheduler.Task('20160718T01','w1','RUNNING')

        self.assertTrue(task.is_running())

    def test_is_not_running(self):
        task = atvscheduler.Task('20160718T01','w1','SCHEDULED')

        self.assertFalse(task.is_running())

    def test_merge_to_payloads(self):

        expected_output = {'payload': {'key3': [4, 5, 6], 'key2': [1, 2, 3],
                             'key1': 'value2'}}

        task = atvscheduler.Task('20160718T01','w1','SCHEDULED')
        task.set_title('Task 1')
        task.set_payload({"key1": "value1", "key2":[1,2,3]})
        
        task.set_payload({"key1": "value2", "key3":[4,5,6]})

        self.assertEquals(task.get_payload_data(), expected_output)

    def test_delete_payload_entry(self):
        expected_output = {'payload': {'key1': 'value1'}}

        task = atvscheduler.Task('20160718T01','w1','SCHEDULED')
        task.set_title('Task 1')
        task.set_payload({"key1": "value1", "key2":[1,2,3]})
        
        task.delete_payload_entry_by_key("key2")

        self.assertEquals(task.get_payload_data(), expected_output)


if __name__ == '__main__':
    unittest.main()