init:
	pip install -r requirements.txt

run:
	FLASK_APP=atvscheduler/service.py flask run

test:
	nosetests tests