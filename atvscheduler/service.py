#!flask/bin/python
from flask import Flask
from flask import jsonify
from flask import request

from task import Task
from scheduler import Scheduler

app = Flask(__name__)
app.scheduler = None

# Initialize the Scheduler
# This function is also adding some tasks in order to have
# some demo data
@app.before_first_request 
def init():
    app.logger.debug('Starting scheduler ...') 
    app.scheduler = Scheduler()

    #TODO: Remove for production
    task = Task('20160718T01','w1','SCHEDULED')
    task.set_title('Task 1')
    task.set_payload({"key1": "value1", "key2":[1,2,3]})
    app.scheduler.add(task)

    task = Task('20160718T01','w1','SCHEDULED')
    task.set_title('Task 2')
    task.set_payload({"key1": "value1", "key2":[1,2,3]})
    app.scheduler.add(task)


@app.route('/api/tasks', methods=['GET'])
def get_tasks():
    app.logger.debug('[get_tasks] new request to show tasks') 
    return jsonify({'tasks': app.scheduler.get_all_tasks_data()})


@app.route('/api/tasks', methods=['POST'])
def create_task():
    if not request.json:
        app.logger.error('[create_task] No request added, not'
                    ' possible to create a task') 
        abort(400)
    else:
        app.logger.debug('[create_task] new request to create a new tasks')
        task = prepare_task_to_be_added(request)
        
        app.logger.debug('[create_task] adding task to the scheduler') 
        app.scheduler.add(task)

        return jsonify(task.get_data()), 201


@app.route('/api/tasks/<int:task_id>', methods=['GET'])
def retrieve_task(task_id):
    app.logger.debug('[retrieve_task] new request to retrieve the'
                ' tasks with id %s', task_id) 

    task = app.scheduler.get(task_id)
    return jsonify(task.get_data()), 201


@app.route('/api/tasks/<int:task_id>', methods=['PATCH'])
def update_task(task_id):
    if not request.json:
        app.logger.error('[update_task] No request added, not possible to'
                    ' modify the task with id %s', task_id) 

        abort(400)
    else:
        app.logger.debug('[update_task] new request to update the tasks'
                    ' with id %s', task_id) 

        modified_task = prepare_task_to_be_added(request)
        task = app.scheduler.get(task_id)

        app.logger.debug('[update_task] updating the task with'
                    ' id %s', task_id)

        task.update_from(modified_task)

        return jsonify(task.get_data()), 201

@app.route('/api/tasks/<int:task_id>', methods=['DELETE'])
def delete_task(task_id):
    
    app.logger.debug('[delete_task] requested to delete the'
                ' task with id %s', task_id) 

    result = app.scheduler.delete(task_id)

    if not result:
        app.logger.debug('[delete_task] was not possible to delete'
                    ' the task with id  %s, is running', task_id)

        return jsonify({'response':'Task is running, you are not allowed'
        ' to delete it'}), 412
    
    return jsonify({'response':'Success'}), 201

@app.route('/api/tasks/<int:task_id>/payload', methods=['POST'])
def update_task_payload(task_id):
    if not request.json:
        app.logger.error('[update_task_payload] No request added, not'
                    ' possible to modify payload for thetask with'
                    ' id %s', task_id) 

        abort(400)
    else:

        app.logger.debug('[update_task_payload] Updating payload for'
                    ' task with id %s', task_id) 

        task = app.scheduler.get(task_id)
        task.set_payload(request.json.get('payload', ""))

        return jsonify(task.get_data()), 201    

@app.route('/api/tasks/<int:task_id>/payload/<string:key>', methods=['DELETE'])
def delete_task_payload(task_id, key):
    
    app.logger.debug('[delete_task_payload] request to delete entry %s for task'
                ' with id %s', key, task_id)

    task = app.scheduler.get(task_id)
    task.delete_payload_entry_by_key(key)

    return jsonify(task.get_data()), 201

def prepare_task_to_be_added(request):
    status = request.json.get('status',"")
    
    if status != "SCHEDULED" and status != "RUNNING" and\
         status != "SUCCEEDED" and status != "FAILED":

            return jsonify({'response':'Status is not valid, should be'
                ' SCHEDULED, RUNNING, SUCCEEDED or FAILED'}), 201

    task = Task(request.json.get('start',""), 
        request.json.get('worker',""),
        request.json.get('status',""))
    task.set_title(request.json.get('title', ""))
    task.set_payload(request.json.get('payload', ""))
    
    return task

if __name__ == '__main__':
    app.run(debug=True)