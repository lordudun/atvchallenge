class Scheduler(object):

    last_id = 0
    tasks = {}

    def __init__(self):
        self.last_id = 0
        self.tasks = {}

    def add(self, task):
        self.last_id += 1
        task.set_id(self.last_id)
        self.tasks[self.last_id] = task
        return task

    def get(self, key):
        return self.tasks.get(key)

    def modify(self, task, key):
        original_task = self.tasks.get(key)
        task.set_id(self.last_id)
        self.tasks[key] = task
        return task

    def delete(self, key):
        task = self.tasks.get(key)
        
        if task.is_running():
            return False

        del self.tasks[key]
        return True

    def get_all_tasks_data(self):
        
        outputFormat = []
        for task in self.tasks:
            outputFormat.append(self.tasks[task].get_data())

        return outputFormat

