class Task(object):
    
    """
    The 'task' is a scheduled job that is executed by a concrete worker.

    Attributes:
        start (required) is of type datetime with seconds resolutions at least. In format YYYYMMDDT01
        title (optional) is of type string
        worker (required) is of type string and is the name of the worker which runs the task
        status (required) is of type string and the allowed values are: "SCHEDULED" or "RUNNING" or "SUCCEEDED" or "FAILED"
        payload (optional) is of type dictionary and stores application layer data

    """
    id = 0
    start = ''
    title = None
    worker = ''
    status = 'FAILED'
    payload = {}

    def __init__(self, start, worker, status):
        
        self.validate_task_values(start, worker, status)
        self.start = start    
        self.worker = worker
        self.status = status
        self.payload = {}

    def set_id(self, id):
        self.id = id

    def set_worker(self, worker):
        self.worker = worker

    def set_title(self, title):
        self.title = title

    def set_start(self, start):
        self.start = start

    def set_status(self, status):

        self.validate_task_status(status)
        self.status = status

    def set_payload(self, payload):
        self.payload = dict(self.payload.items() + payload.items())

    def update_from(self, task):

        self.validate_task_values(task.start, task.worker, task.status)
        self.status = task.status
        self.start = task.start
        self.title = task.title
        self.worker = task.worker
        self.payload = task.payload

    def is_running(self):
        return self.status == "RUNNING"

    def get_data(self):
        return {'id': self.id, 'start': self.start, 'title': self.title,
                 'worker': self.worker, 'status': self.status, 'payload':
                  self.payload}

    def get_payload_data(self):
        return {'payload': self.payload}

    def delete_payload_entry_by_key(self, key):
        del self.payload[key]

    def validate_task_values(self, start, worker, status):
        
        self.validate_task_status(status)

        if start == None or start == "":
            raise ValueError('Start should be a valid date in the future')

        if worker == None or worker == "":
            raise ValueError('Worker should not be empty')

    def validate_task_status(self, status):

        if not self.is_valid_status(status):
           raise ValueError('The status is not valid, should be'
                            ' "SCHEDULED" or "RUNNING" or "SUCCEEDED"'
                            ' or "FAILED"')

    def is_valid_status(self, status):
        return status == "SCHEDULED" or status == "RUNNING" or\
                 status == "SUCCEEDED" or status == "FAILED"

    def __eq__(self, other): 
        return self.id == other.id and self.start == other.start and\
                 self.title == other.title and\
                 self.worker == other.worker and\
                  self.status == other.status and\
                   self.payload == other.payload
