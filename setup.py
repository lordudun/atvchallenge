# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

setup(
    name='atv_challenge',
    version='1.0.0',
    description='Python Challenge',
    long_description=readme,
    author='Oriol del Barrio',
    author_email='oriol@lordudun.es',
    url='https://github.com/lordudun/atvchallenge',
    packages=find_packages(exclude=('tests', 'docs'))
)